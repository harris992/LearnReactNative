/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';
export default class WaitingLeaf extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.textPromptStyle}>
                    登录使用手机号：{this.props.phoneNumber}
                </Text>
                <Text style={styles.textPromptStyle}>
                    登录使用密码：{this.props.userPW}
                </Text>
                <Text style={styles.bigTextPrompt} onPress={() => this.onGobackPressed()}>
                    返回
                </Text>
            </View>
        );
    }

    onGobackPressed() {
        //返回前一个页面
        this.props.navigator.pop();
        //重新加载新页面
        // this.props.navigator.replace({
        //     name: "login"
        // })
    }

    userPressAddressBook() {

    }
}
/*属性确认声明代码和必需的*/
WaitingLeaf.propTypes = {
    phoneNumber: React.PropTypes.string,
    userPW: React.PropTypes.string.isRequired
}
/*设置属性默认值*/
WaitingLeaf.defaultProps = {
    phoneNumber: "123456",
    userPW: '45678'
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    textPromptStyle: {
        // margin: widthOfMargin,
        fontSize: 20
    },
    bigTextPrompt: {
        width: 300,
        backgroundColor: 'gray',
        color: 'white',
        textAlign: 'center',
        fontSize: 60
    },
});

