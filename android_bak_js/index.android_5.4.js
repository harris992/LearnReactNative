/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    TouchableNativeFeedback,
    TouchableOpacity,
    TouchableHighlight
} from 'react-native';

export default class LearnRN extends Component {
    constructor(props) {
        super(props);
    }

    /*componentWillMount() {
     let imageSouce = {
     uri: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1321604390,4156419709&fm=27&gp=0.jpg'
     };
     Image.prefetch(imageSouce).then(
     (resulet) => {
     }
     ).catch(
     (error) => {
     console.log('error when prefetch.');
     }
     )
     // this.image1 = require('./iamge/girl.jpg');
     }*/

    render() {
        return (
            <View style={styles.container}>

                <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('red', false)}>
                    <View style={styles.button}></View>
                </TouchableNativeFeedback>
                <TouchableOpacity activeOpacity={0.5}>
                    <View style={styles.button}></View>
                </TouchableOpacity>
                <TouchableHighlight>
                    <View style={styles.button2}></View>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    button: {
        width: 300,
        height: 70,
        backgroundColor: 'grey'
    },
    button2: {
        width: 120,
        height: 70,
        backgroundColor: 'grey'
    }
});

AppRegistry.registerComponent('LearnRN', () => LearnRN);
