/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image
} from 'react-native';

export default class LearnRN extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var imageAddress = 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1321604390,4156419709&fm=27&gp=0.jpg';
        var imageName = './image/tx.jpg';
        return (
            <View style={styles.container}>

                {/*<Image style={styles.imageStyle} source={{uri: imageName}}/>*/}
                <Image style={[styles.imageStyle, {width: 200, height: 200}]} source={require('./image/tx.jpg')}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        backgroundColor: 'grey'
    },
    imageStyle: {
        flexDirection: 'row',

        alignItems: 'center',
        borderColor: 'green',
        borderRadius: 100,
    }
});

AppRegistry.registerComponent('LearnRN', () => LearnRN);
