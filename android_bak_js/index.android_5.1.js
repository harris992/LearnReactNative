/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View
} from 'react-native';

export default class LearnRN extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.firstRow}>
                    <View style={styles.test1}/>
                    <View style={styles.test2}/>
                    <View style={styles.test3}/>
                    {/*<View style={styles.test1}/>
                    <View style={styles.test2}/>
                    <View style={styles.test3}/>*/}
                </View>

                <View style={styles.firstRow2}>
                    <View style={styles.test1}/>
                    <View style={styles.test2}/>
                    <View style={styles.test3}/>
                </View>

                <View style={styles.firstRow3}>
                    <View style={styles.test1}/>
                    <View style={styles.test2}/>
                    <View style={styles.test3}/>
                </View>

                <View style={styles.firstRow4}>
                    <View style={styles.test1}/>
                    <View style={styles.test2}/>
                    <View style={styles.test3}/>
                </View>

                <View style={styles.firstRow5}>
                    <View style={styles.test1}/>
                    <View style={styles.test2}/>
                    <View style={styles.test3}/>
                </View>

                <View style={styles.testPosition}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    firstRow: {
        height: 80,
        top: 40,
        backgroundColor: 'black',
        flexDirection: 'row',//row,row-reverse,column,column-reverse
        alignItems: 'flex-end',
        justifyContent: 'center',
        flexWrap: 'nowrap'
    },
    test1: {
        width: 68,
        height: 24,
        backgroundColor: 'blue',
    },
    test2: {
        width: 40,
        height: 24,
        backgroundColor: 'red',
        alignSelf: 'flex-start'
    },
    test3: {
        width: 100,
        height: 24,
        backgroundColor: 'green',
    },
    testPosition: {
        width: 60,
        height: 60,
        backgroundColor: 'grey',
        // position: 'relative',
        position: 'absolute',
        top: 150,
        right: 50
    },

    firstRow2: {
        height: 80,
        top: 40,
        backgroundColor: 'black',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexWrap: 'nowrap'
    },
    firstRow3: {
        height: 80,
        top: 40,
        backgroundColor: 'black',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexWrap: 'nowrap'
    },
    firstRow4: {
        height: 80,
        top: 40,
        backgroundColor: 'black',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'nowrap'
    },
    firstRow5: {
        height: 80,
        top: 40,
        backgroundColor: 'black',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexWrap: 'nowrap'
    },

});

AppRegistry.registerComponent('LearnRN', () => LearnRN);
