/**
 * 5.2.6
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Text
} from 'react-native';

export default class LearnRN extends Component {
    _onLayout(event) {
        {
            let {x, y, width, height} = event.nativeEvent.layout;
            console.log('_onLayout1)width:' + width, ' height:' + height, ' x:' + x, ' y:' + y);
        }
        let Dimensions = require('Dimensions');
        let {width, height} = Dimensions.get('window');
        console.log('_onLayout2)width:' + width, ' height:' + height);
        console.log('\r\n');
    }

    _onLayoutText(event) {
        let {x, y, width, height} = event.nativeEvent.layout;
        console.log('_onLayoutText)width:' + width, ' height:' + height, ' x:' + x, ' y:' + y);
        console.log('\r\n');
    }

    render() {
        return (
            <View style={styles.container} onLayout={this._onLayout}>
                <Text style={styles.welcome} onLayout={this._onLayoutText}>
                    Welcome to React Native!
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        backgroundColor: 'grey'
    }
});

AppRegistry.registerComponent('LearnRN', () => LearnRN);
