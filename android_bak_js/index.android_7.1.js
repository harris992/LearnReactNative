/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Text
} from 'react-native';

export default class LearnRN extends Component {

    constructor(props) {
        super(props);
    }

    /**
     * 只执行一次
     * render()函数执行前被执行
     */
    componentWillMount() {

    }

    /**
     * 只执行一次
     * 初始渲染完成后马上执行
     */
    componentDidMount() {

    }

    /**
     * 初始渲染完成后，当RN组件接收到新的props时被调用
     * 如果新的props会导致页面渲染，将在渲染之前执行
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {

    }

    /**
     * 阻止无必要的重新渲染
     * return boolean
     */
    shouldComponentUpdate(nextProps, nextState) {

    }

    /**
     * RN组件初始化完成后，RN重新渲染前调用
     * 不能在这个函数中改变状态机变量的值
     * @param nextProps
     * @param nextState
     */
    componentWillUpdate(nextProps, nextState) {

    }

    /**
     * RN组件初始化完成后，RN重新渲染完成后调用
     * @param nextProps
     * @param nextState
     */
    componentDidUpdate(nextProps, nextState) {

    }

    /**
     * RN组件被卸载前执行，释放资源时使用
     */
    componentWillUnmount() {

    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Welcome to React Native!
                </Text>
                <Text style={styles.instructions}>
                    To get started, edit index.android.js
                </Text>
                <Text style={styles.instructions}>
                    Double tap R on your keyboard to reload,{'\n'}
                    Shake or press menu button for dev menu
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});


AppRegistry.registerComponent('LearnRN', () => LearnRN);
