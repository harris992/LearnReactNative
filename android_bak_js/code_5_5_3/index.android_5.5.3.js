/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet
} from 'react-native';
import {Navigator} from 'react-native-deprecated-custom-components';
import Page1 from './page/Page1'
import Page2 from './page/Page2'
import Page3 from './page/Page3'
import Page4 from './page/Page4'

export default class LearnRN extends Component {
    constructor(props) {
        super(props);
        this.renderScene = this.renderScene.bind(this);
    }

    configureScene(route) {
        return Navigator.SceneConfigs.FadeAndroid;
    }

    renderScene(router, navigator) {
        switch (router.name) {
            case "Page1":
                return <Page1 navigator={navigator}/>;
            case "Page2":
                return <Page2 navigator={navigator}/>;
            case "Page3":
                return <Page3 navigator={navigator}/>;
            case "Page4":
                return <Page4 navigator={navigator}/>;
        }
    }

    render() {
        return (
            <Navigator initialRoute={{name: 'Page1'}} configureScene={this.configureScene}
                       renderScene={this.renderScene}></Navigator>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        // borderWidth: 1,
        backgroundColor: 'white'
    },
    button: {
        width: 300,
        height: 70,
        backgroundColor: 'grey'
    },
    button2: {
        width: 120,
        height: 70,
        backgroundColor: 'grey'
    }
});

AppRegistry.registerComponent('LearnRN', () => LearnRN);
