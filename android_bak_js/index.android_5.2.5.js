/**
 * pointerEvents
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Text
} from 'react-native';

export default class LearnRN extends Component {
    constructor(props) {
        super(props);
        this.state = {bigButtonPointerEvents: null};
        this.onBigButtonPressed = this.onBigButtonPressed.bind(this);
        this.onSmallButtonPressed = this.onSmallButtonPressed.bind(this);
    }

    onBigButtonPressed(event) {
        console.log('Big Button pressed');
    }

    onSmallButtonPressed(event) {
        if (this.state.bigButtonPointerEvents === null) {

            console.log('big button will not responde.');
            this.setState({bigButtonPointerEvents: 'none'});
            return;
        }
        console.log('big button willresponde.');
        this.setState({bigButtonPointerEvents: null});
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.sButtonStyle} onPress={this.onSmallButtonPressed}>
                    Small button
                </Text>
                <View style={styles.bButtonStyle}
                      pointerEvents={this.state.bigButtonPointerEvents}>
                    <Text style={{flex: 1, fontSize: 20}}
                          onPress={this.onBigButtonPressed}
                    >
                        BigButton
                    </Text>
                </View>


                {/*<Text style={styles.bButtonStyle} onPress={this.onBigButtonPressed}*/}

                {/*pointerEvents={this.state.bigButtonPointerEvents}>*/}
                {/*Big button*/}
                {/*</Text>*/}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    sButtonStyle: {
        fontSize: 20,
        left: 130,
        top: 50,
        width: 150,
        height: 35,
        backgroundColor: 'grey'
    },
    bButtonStyle: {
        fontSize: 20,
        left: 130,
        top: 130,
        width: 150,
        height: 70,
        backgroundColor: 'green'
    }
});

AppRegistry.registerComponent('LearnRN', () => LearnRN);
