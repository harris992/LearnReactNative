/**
 * 5.2.1 View组件的颜色与边框
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View
} from 'react-native';

export default class LearnRN extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.welcome} opacity={0}/>
                <View style={styles.welcome} opacity={0.1}/>
                <View style={styles.welcome} opacity={0.25}/>
                <View style={styles.welcome} opacity={0.5}/>
                <View style={styles.welcome} opacity={1}/>
                <View style={styles.welcome} opacity={5}/>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    welcome: {
        width: 50,
        height: 50,
        borderWidth: 1,
        backgroundColor: 'white',
        borderRadius: 25,
        elevation: 10,
    },
    test1: {
        width: 68,
        height: 24,
        backgroundColor: 'blue',
    },
    test2: {
        width: 40,
        height: 24,
        backgroundColor: 'red',
        alignSelf: 'flex-start'
    },
    test3: {
        width: 100,
        height: 24,
        backgroundColor: 'green',
    },
    testPosition: {
        width: 60,
        height: 60,
        backgroundColor: 'grey',
        // position: 'relative',
        position: 'absolute',
        top: 150,
        right: 50
    },

    firstRow2: {
        height: 80,
        top: 40,
        backgroundColor: 'black',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexWrap: 'nowrap'
    },
    firstRow3: {
        height: 80,
        top: 40,
        backgroundColor: 'black',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexWrap: 'nowrap'
    },
    firstRow4: {
        height: 80,
        top: 40,
        backgroundColor: 'black',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'nowrap'
    },
    firstRow5: {
        height: 80,
        top: 40,
        backgroundColor: 'black',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexWrap: 'nowrap'
    },

});

AppRegistry.registerComponent('LearnRN', () => LearnRN);
