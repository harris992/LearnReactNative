/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TextInput
} from 'react-native';

let widthOfMargin = Dimensions.get('window').width * 0.05;
export default class LearnRN extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputedNum: '',
            inputedPW: ''
        };
        this.updatePW = this.updatePW.bind(this);
    }

    updateNum(newText) {

        this.setState((oldState) => {
            for (let aName in oldState) {
                console.log('aName:' + aName);
                console.log('oldState[' + aName + ']:' + oldState[aName]);
            }
            return {
                inputedNum: newText,
                aBrandnewStateVariable: 'I am a new variable.'
            };
        }, this.changeNumDone);
    };

    changeNumDone = () => {
        // this.forceUpdate();//强制渲染
        console.log('React Native has changged inputed Num');
    };

    updatePW(newText) {
        this.setState(() => {
            return {
                inputedPW: newText,
            };
        });
    }

//判断是否渲染
    shouldComponentUpdate() {
        if (this.state.inputedNum.length < 3) return false;
        return true;
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput style={styles.textInputStyle} placeholder={'请输入手机号'}
                           onChangeText={(newText) => this.updateNum(newText)}/>

                <Text style={styles.textPromptStyle}>
                    请输入手机号：{this.state.inputedNum}
                </Text>
                <TextInput style={styles.textInputStyle} placeholder={'请输入密码'} secureTextEntry={true}
                           onChangeText={this.updatePW}/>
                <Text style={styles.bigTextPrompt}>
                    确 定
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: 'white',
    },
    textInputStyle: {
        margin: widthOfMargin,
        backgroundColor: 'gray',
        // height:30,
        fontSize: 20
    },
    textPromptStyle: {
        margin: widthOfMargin,
        fontSize: 20
    },
    bigTextPrompt: {
        margin: widthOfMargin,
        backgroundColor: 'gray',
        color: 'white',
        textAlign: 'center',
        fontSize: 30
    },
});

AppRegistry.registerComponent('LearnRN', () => LearnRN);
