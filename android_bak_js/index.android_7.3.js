/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Text,
    AsyncStorage
} from 'react-native';

/**
 * 引入json文件
 */
let constantData = require('./data/simple.json');
let newJSONString = JSON.stringify(constantData);
let anotherData = JSON.parse(newJSONString);

console.log(constantData);
console.log(newJSONString);
console.log(anotherData);
/**
 * 存储单个
 */
AsyncStorage.setItem('name', '张三');
AsyncStorage.setItem('name', '张三2').then(() => {
//定义操作成功的处理函数
}).catch((error) => {
//定义操作失败的处理函数
    console.log('error:' + error.message);
});

/**
 * 存储多个
 */
AsyncStorage.multiSet([['a', '1'], ['b', '2']]);
/**
 * 删除数据
 */
// AsyncStorage.removeItem('name');
/**
 * 清空数据
 */
AsyncStorage.clear();
AsyncStorage.multiRemove(['a', 'b']);
/**
 * 读取数据
 */
AsyncStorage.getItem('name').then((result) => {
    console.log(result);
}).catch((error) => {
//定义操作失败的处理函数
    console.log('error:' + error.message);
});
AsyncStorage.getAllKeys().then((keys) => {
    let arrayLen = keys.length;
    for (let i = 0; i < arrayLen; i++) {
        console.log('key ' + i + ':' + keys[i]);
        AsyncStorage.getItem(keys[i]).then((result) => {
            console.log('key ' + i + ':' + keys[i] + ' data:' + result);
        })
    }
});
AsyncStorage.multiGet(['a', 'b']).then((result) => {
    console.log(result[0][0]);
    console.log(result[0][1]);
    console.log(result[1][0]);
    console.log(result[1][1]);
});
/**
 * 取消操作
 */
// AsyncStorage.flushGetRequests().then(() => {
//
// }).catch(() => {
//
// });

export default class LearnRN extends Component {

    constructor(props) {
        super(props);
        console.log(constantData)
    }


    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Welcome to React Native!
                </Text>
                <Text style={styles.instructions}>
                    To get started, edit index.android.js
                </Text>
                <Text style={styles.instructions}>
                    Double tap R on your keyboard to reload,{'\n'}
                    Shake or press menu button for dev menu
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});


AppRegistry.registerComponent('LearnRN', () => LearnRN);
