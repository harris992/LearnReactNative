/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text
} from 'react-native';
import NaviBar from '../NaviBar'
export default class Page1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputedNum: '',
            inputedPW: ''
        };
        this.onNaviBarPress = this.onNaviBarPress.bind(this);
        this.naviStatus = [0, 1, 0, 0];
    }


    onNaviBarPress(aNumber) {
        switch (aNumber) {
            case 0:
                this.props.navigator.replace({name: 'Page1'});
                return;
            case 1:
                return;
            case 2:
                this.props.navigator.replace({name: 'Page3'});
                return;
            case 3:
                this.props.navigator.replace({name: 'Page4'});
                return;
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <NaviBar naviBarStatus={this.naviStatus} onNaviBarPress={this.onNaviBarPress}/>
                <View style={styles.whatLeft}>
                    <Text>
                        栏目内容2
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        // borderWidth: 1,
        // backgroundColor: 'white'
    },
    whatLeft: {
        flex: 1,
        borderTopWidth: 1,
        // backgroundColor: 'white'
    },
});

