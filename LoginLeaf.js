/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    TextInput,
    Alert,
    DeviceEventEmitter
} from 'react-native';
let widthOfMargin = Dimensions.get('window').width * 0.05;
export default class LearnLeaf extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputedNum: '',
            inputedPW: ''
        };
        this.updatePW = this.updatePW.bind(this);
        this.jumpToWaiting = this.jumpToWaiting.bind(this);
    }

    updateNum(newText) {
        this.setState((oldState) => {
            for (let aName in oldState) {
                console.log('aName:' + aName);
                console.log('oldState[' + aName + ']:' + oldState[aName]);
            }
            return {
                inputedNum: newText,
                aBrandnewStateVariable: 'I am a new variable.'
            };
        }, this.changeNumDone);
    }

    changeNumDone() {
        // this.forceUpdate();//强制渲染
        console.log('React Native has changged inputed Num');
    }

    updatePW(newText) {
        this.setState(() => {
            return {
                inputedPW: newText,
            };
        });
    }

//判断是否渲染
    shouldComponentUpdate() {
        // if (this.state.inputedNum.length < 3) {
        //     return false;
        // } else {
        return true;
        // }

    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput style={styles.textInputStyle} placeholder={'请输入手机号qq'}
                           onChangeText={(newText) => this.updateNum(newText)}/>

                <Text style={styles.textPromptStyle}>
                    请输入手机号：{this.state.inputedNum}
                </Text>
                <TextInput style={styles.textInputStyle} placeholder={'请输入密码'} secureTextEntry={true}
                           onChangeText={this.updatePW}/>
                <Text style={styles.bigTextPrompt} onPress={() => this.userPressConfirm()}>
                    确 定
                </Text>
                <Text style={styles.bigTextPrompt} onPress={() => this.userPressAddressBook()}>
                    通讯录
                </Text>
            </View>
        );
    }

    /*保留页面并跳转新页面*/
    // userPressConfirm() {
    //     this.props.navigator.push({
    //         phoneNumber: this.state.inputedNum,
    //         userPW: this.state.inputedPW,
    //         name: 'waiting',
    //     })
    // }
    /*不保留新页面并跳转新开页面*/
    // jumpToWaiting() {
    //     this.props.navigator.replace({
    //         phoneNumber: this.state.inputedNum,
    //         userPW: this.state.inputedPW,
    //         name: 'waiting',
    //     })
    // }
    userPressConfirm() {
        Alert.alert('提示', '确定使用' + this.state.inputedNum + '号码登录吗？',
            [
                {
                    text: '取消', onPress: (() => {
                }), style: 'cancel'
                },
                {text: '确定', onPress: this.jumpToWaiting}
            ]
        );
    }

    jumpToWaiting() {
        this.props.navigator.push({
            phoneNumber: this.state.inputedNum,
            userPW: this.state.inputedPW,
            name: 'waiting',
        })
    }

    userPressAddressBook() {
        //发送消息方式
        // DeviceEventEmitter.addListener('AndroidToRNMessage', this.handleAndroidMessage.bind(this));//监听原生消息
        // var {NativeModules} = require('react-native');
        // let ExampleInterface = NativeModules.ExampleInterface;
        // NativeModules.ExampleInterface.HandleMessage('textMessage');

        // DeviceEventEmitter.addListener('AndroidToRNMessage', this.handleAndroidMessage.bind(this));//监听原生消息
        //Promise
        var {NativeModules} = require('react-native');
        let ExampleInterface = NativeModules.ExampleInterface;//注册的ExampleInterface
        //注册的ExampleInterface里的HandleMessage
        NativeModules.ExampleInterface.HandleMessage('textMessage').then(
            (result) => {
                console.log(result);
                let aObj = JSON.parse(result);
                this.setState({inputedNum: aObj.peerNumber});//将用户选择的号码显示在RN界面上
            }
        ).catch(
            (error) => {
                console.log(error);
                console.log(error.message);
                console.log(error.code);
            }
        );

    }

    handleAndroidMessage(aMessage) {
        console.log('handleAndroidMessage:' + aMessage);
        let aObj = JSON.parse(aMessage);
        this.setState({inputedNum: aObj.peerNumber});//将用户选择的号码显示在RN界面上

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: 'white',
    },
    textInputStyle: {
        margin: widthOfMargin,
        backgroundColor: 'gray',
        // height:30,
        fontSize: 20
    },
    textPromptStyle: {
        margin: widthOfMargin,
        fontSize: 20
    },
    bigTextPrompt: {
        margin: widthOfMargin,
        backgroundColor: 'gray',
        color: 'white',
        textAlign: 'center',
        fontSize: 30
    },
});

