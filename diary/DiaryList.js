/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, {Component} from 'react';
import {
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image,
    StatusBar,
    ListView,
    ProgressBarAndroid
} from 'react-native';

let angryMood = require('../image/tx.jpg');

import MCV from '../diary/MCV';

export default class DiaryList extends Component {

    constructor(props) {
        super(props);
        debugger;
        this.state = {
            diaryListDataSource: new ListView.DataSource({
                rowHasChanged: (oldRow, newRow) => {
                    oldRow !== newRow
                }
            }),
            progress: 0
        };
        this.updateSearchKeyword = this.updateSearchKeyword.bind(this);
        this.renderListItem = this.renderListItem.bind(this);
        this.progressTimer = null;
        this.updateProgress = this.updateProgress.bind(this);
    }

    componentDidMount() {
        this.updateProgress();
    }

    componentWillMount() {
        if (this.props.diaryList === null) return;
        this.setState(
            {diaryListDataSource: this.state.diaryListDataSource.cloneWithRows(this.props.diaryList)}
        );
    }

    componentWillReceiveProps(nextProps) {
        this.setState(
            {diaryListDataSource: this.state.diaryListDataSource.cloneWithRows(nextProps.diaryList)}
        );
    }

    updateSearchKeyword(newWord) {
        this.props.searchKeyword(newWord);

    }

    updateProgress() {
        var progress = (this.state.progress + 0.025) % 1;
        this.setState({progress});
        this.progressTimer = window.requestAnimationFrame(
            () => this.updateProgress()
        );
    }

    componentWillUnmount() {
        window.cancelAnimationFrame(this.progressTimer);
    }

    // renderSectionHeader (sectionData,sectionID) {
    //     return(
    //         <View style={MCV.section}>
    //             <Text style={MCV.sectionText}>{sectionData}</Text>
    //         </View>
    //     )
    // }
    renderHeader() {
        return (
            <View style={MCV.section}>
                <Text style={MCV.sectionText}>美丽的头</Text>
            </View>
        )
    }

    render() {

        return (
            <View style={MCV.container}>


                <StatusBar hidden={false}/>
                <ProgressBarAndroid styleAttr="Horizontal" indeterminate={false} style={{top: 0, width: 400}}
                                    progress={this.state.progress}/>
                <View style={MCV.firstRow}>

                    <View style={{borderWidth: 1}}>
                        <TextInput autoCapitalize="none" placeholder='请输入搜索关键字'
                                   clearButtonMode="while-editing"
                            // onChangeText={this.updateSearchKeyword}
                                   style={MCV.searchBarTextInput}/>
                    </View>
                    <TouchableOpacity onPress={this.props.writeDiary}>
                        <Text style={MCV.middleButton}>写日记</Text>
                    </TouchableOpacity>

                </View>

                {/*<View style={{top:40}}>
                    <ActivityIndicator animating={true} color={'blue'} size={'large'}/>
                </View>*/}
                {(
                    (this.props.diaryList.length !== 0) ? (
                        <ListView dataSource={this.state.diaryListDataSource}
                                  renderRow={this.renderListItem}
                                  renderSectionHeader={this.renderSectionHeader}
                                  renderHeader={this.renderHeader}></ListView>
                    ) : (
                        <View style={{flex: 1, justifyContent: 'center'}}>
                            <Text style={{fontSize: 18}}>您还没有写日记哦。</Text>
                        </View>
                    )
                )}
                {/*<View style={MCV.diaryAbstractList}>
                    <View style={MCV.secondRow}>
                        <Image style={MCV.moodStyle} source={angryMood}/>
                        <View style={MCV.subViewInReader}>
                            <TouchableOpacity onPress={this.props.selectLististItem}>
                                <Text style={MCV.textInReader}>
                                    某变量记录假日记列表标题
                                </Text>
                            </TouchableOpacity>
                            <Text style={MCV.textInReader}>
                                某变量记录假日记列表时间
                            </Text>
                        </View>
                    </View>
                </View>*/}
            </View>
        );
    }

    renderListItem(log, sectionID, rowID) {
        return (
            <TouchableOpacity onPress={() => this.props.selectLististItem(rowID)}>
                <View style={MCV.secondRow}>
                    <Image style={MCV.moodStyle} source={log.mood}/>
                    <View style={MCV.subViewInReader}>
                        <Text style={MCV.textInReader}>
                            {log.title}
                        </Text>
                        <Text style={MCV.textInReader}>
                            {log.time}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}


