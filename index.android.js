/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
} from 'react-native';
import DiaryList from './diary/DiaryList';
import DiaryWriter from './diary/DiaryWriter';
import DiaryReader from './diary/DiaryReader';
import DataHandler from './diary/DataHandler';

export default class LearnRN extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uiCode: 1,
            diaryList: [],
            diaryMood: null,
            diaryTime: '读取中...',
            diaryTitle: '读取中...',
            diaryBody: '读取中...',
        };
        this.bindAllMyFunction();//执行回调函数绑定操作
        DataHandler.getAllTheDiary().then(
            (result) => {
                this.setState({diaryList: result});
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )
    }

    bindAllMyFunction() {
        this.selectLististItem = this.selectLististItem.bind(this);
        this.writeDiary = this.writeDiary.bind(this);
        this.returnPressed = this.returnPressed.bind(this);
        this.saveDiaryAndReturn = this.saveDiaryAndReturn.bind(this);
        this.readingPreviousPressed = this.readingPreviousPressed.bind(this);
        this.readingNextPressed = this.readingNextPressed.bind(this);
    }

    //阅读日记界面请求读上一篇日记的处理函数
    readingPreviousPressed() {
        let previousDiary = DataHandler.getPreviousDiary();
        if (previousDiary === null) return;//已经显示的是第一篇日记
        this.setState(previousDiary);//显示上一篇日记
    }

    //阅读日记界面请求读下一篇日记的处理函数
    readingNextPressed() {
        let nextDiary = DataHandler.getNextDiary();
        if (nextDiary === null) return;//已经到最后一篇日记
        this.setState(nextDiary);//显示下一篇日记
    }

    //阅读日记界面、写日记界面返回日记列表界面的处理函数
    returnPressed() {
        this.setState({uiCode: 1});
    }

    //写日记界面保存日记并返回日记列表界面的处理函数
    saveDiaryAndReturn(newDiaryMood, newDiaryBody, newDiaryTitile) {
        DataHandler.saveDiary(newDiaryMood, newDiaryBody, newDiaryTitile).then(
            (result) => {
                this.setState(result);
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        );
    }

    //写日记按钮被按下时的处理函数
    writeDiary() {
        this.setState(() => {
            return {
                uiCode: 3
            };
        });
    }

    //搜索处理函数
    searchKeyword(keyword) {
        console.log('search keyword is:' + keyword);
    }

    //日记列表中某条记录被选中时的处理函数
    selectLististItem(aIndex) {
        let rValue = DataHandler.getDiaryAtIndex(aIndex);
        this.setState(rValue);
    }

    showDiaryList() {
        return (
            <DiaryList fakeListTitle={this.state.diaryTitle}
                       fakeListTime={this.state.diaryTime}
                       fakeListMood={this.state.diaryMood}
                       selectLististItem={this.selectLististItem}
                       searchKeyword={this.searchKeyword}
                       diaryList={this.state.diaryList}
                       writeDiary={this.writeDiary}/>
        );
    }

    showDiaryWriter() {
        return (<DiaryWriter returnPressed={this.returnPressed}
                             saveDiary={this.saveDiaryAndReturn}/>
        );
    }

    showDiaryReader() {
        return (<DiaryReader returnToDiaryList={this.returnPressed}
                             diaryTitle={this.state.diaryTitle}
                             diaryTime={this.state.diaryTime}
                             diaryMood={this.state.diaryMood}
                             readingPreviousPressed={this.readingPreviousPressed}
                             returnPressed={this.returnPressed}
                             readingNextPressed={this.readingNextPressed}
                             diaryBody={this.state.diaryBody}/>
        );
    }

    render() {
        if (this.state.uiCode === 1) return this.showDiaryList();
        if (this.state.uiCode === 2) return this.showDiaryReader();
        if (this.state.uiCode === 3) return this.showDiaryWriter();
    }
}

AppRegistry.registerComponent('LearnRN', () => LearnRN);
